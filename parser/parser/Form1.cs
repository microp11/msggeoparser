﻿/*
 * microp11 2019
 * 
 * This file is part of MsgGeoParser.
 * 
 * MsgGeoParser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MsgGeoParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MsgGeoParser.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using System;
using System.Windows.Forms;

namespace parser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (CoordinatesList.TryParseCoordinates(richTextBox1.Text))
            {
                foreach (var coordinate in CoordinatesList.List)
                {
                    richTextBox3.AppendText(coordinate.Value);
                    richTextBox3.AppendText(Environment.NewLine);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            richTextBox3.Clear();
        }
    }
}
