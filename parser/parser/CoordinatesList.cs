﻿/*
 * microp11 2019
 * 
 * This file is part of MsgGeoParser.
 * 
 * MsgGeoParser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MsgGeoParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MsgGeoParser.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace parser
{
    public static class CoordinatesList
    {
        private static readonly string _pattern = "(\\d{1,3})(-| |.)(\\d{1,3})(((.|,)(\\d{1,3})( *))*)(N|S)( *)(-*)( *)(\\d{1,3})(-| |.)(\\d{1,3})(((.|,)(\\d{1,3})( *))*)(E|W)";
        private static readonly MatchEvaluator _evaluator = new MatchEvaluator(Evaluator);

        public static List<Coordinates> List { get; set; } = new List<Coordinates>();

        public static bool TryParseCoordinates(string input)
        {
            List.Clear();
            bool result = true;
            try
            {
                Regex.Replace(input, _pattern, _evaluator);
            }
            catch
            {
                result = false;
            }
            return result;
        }

        private static string Evaluator(Match m)
        {
            try
            {
                Coordinates coordinates = new Coordinates();

                //latitude
                int.TryParse(m.Groups[1].Value, out int degrees);
                coordinates.LatDegrees = degrees;

                int.TryParse(m.Groups[3].Value, out int minutes);
                coordinates.LatMinutes = minutes;

                int.TryParse(m.Groups[7].Value, out int seconds);
                coordinates.LatSeconds = seconds;

                coordinates.LatHemisphere = (m.Groups[9].Value.ToUpper() == "N") ? Coordinates.NORTHERN : Coordinates.SOUTHERN;

                //longitude
                int.TryParse(m.Groups[13].Value, out degrees);
                coordinates.LonDegrees = degrees;

                int.TryParse(m.Groups[15].Value, out minutes);
                coordinates.LonMinutes = minutes;

                int.TryParse(m.Groups[19].Value, out seconds);
                coordinates.LonSeconds = seconds;

                coordinates.LonHemisphere = (m.Groups[21].Value.ToUpper() == "E") ? Coordinates.EASTERN : Coordinates.WESTERN;

                coordinates.Value = m.Value;

                List.Add(coordinates);

                return m.Value;
            }
            catch
            {
                return "";
            }
        }
    }

    public class Coordinates
    {
        public static int NORTHERN = 0;
        public static int SOUTHERN = 1;
        public static int EASTERN = 2;
        public static int WESTERN = 3;
        public string Value { get; set; }

        public int LatDegrees { get; set; }
        public int LatMinutes { get; set; }
        public int LatSeconds { get; set; }
        public int LatHemisphere { get; set; }

        public int LonDegrees { get; set; }
        public int LonMinutes { get; set; }
        public int LonSeconds { get; set; }
        public int LonHemisphere { get; set; }
    }
}
